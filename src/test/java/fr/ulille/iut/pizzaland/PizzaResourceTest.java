package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.PathParam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
    private PizzaDao dao;
    private IngredientDao ingredientDao;

    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();

        return new ApiV1();

    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
    // base de données
    // et les DAO

    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
        dao = BDDFactory.buildDao(PizzaDao.class);
        dao.createTableAndIngredientAssociation();
        
        ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        ingredientDao.createTable();
        
    	IngredientCreateDto test1 = new IngredientCreateDto();
    	test1.setName("Test1");
    	IngredientCreateDto test2 = new IngredientCreateDto();
    	test2.setName("Test2");
        target("/ingredients").request().post(Entity.json(test1));
        target("/ingredients").request().post(Entity.json(test2));
    }

    @After
    public void tearEnvDown() throws Exception {
    	dao.dropTable();
    }

    @Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(0, pizzas.size());
    }
    
    @Test
    public void testGetAllList() {

        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setIngredients(ingredients);
        dao.insertPizzaWithIngredients(pizza);


        Response response = target("/pizzas").request().get();


        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });

        assertEquals(pizza, Pizza.fromDto(pizzas.get(0)));
    }
    @Test
    public void testGetExistingPizza() {
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setIngredients(ingredients);
        dao.insertPizzaWithIngredients(pizza);

        Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());


        Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
        assertEquals(pizza, result);
    }

    @Test
    public void testGetNotExistingPizza() {
      Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
      assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
    }

    @Test
    public void testCreatePizzaWithIngredients() {
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

        PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("Chorizo");
        pizzaCreateDto.setIngredients(ingredients);

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
    }

    @Test
    public void testCreateSamePizza() {
    	IngredientCreateDto test1 = new IngredientCreateDto();
    	test1.setName("Test1");
    	IngredientCreateDto test2 = new IngredientCreateDto();
    	test2.setName("Test2");
        target("/ingredients").request().post(Entity.json(test1));
        target("/ingredients").request().post(Entity.json(test2));
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setIngredients(ingredients);
        dao.insertPizzaWithIngredients(pizza);

        PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreatePizzaWithoutName() {
    	PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    @Test
    public void testCreatePizzaWithoutIngredients() {
    	PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
    	pizzaCreateDto.setName("test");

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }
    @Test
    public void testCreatePizzaWithUnexistingIngredients() {
    	PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
    	pizzaCreateDto.setName("test");
    	List<Ingredient> ingredients = new ArrayList<>();
    	ingredients.add(new Ingredient("unexisting"));
    	pizzaCreateDto.setIngredients(null);

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }   

    @Test
    public void testDeleteExistingPizza() {
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setIngredients(ingredients);
        dao.insertPizzaWithIngredients(pizza);

      Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = dao.findById(pizza.getId());
      assertEquals(result, null);
   }

   @Test
   public void testDeleteNotExistingPizza() {
     Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }

   @Test
   public void testGetPizzaName() {
       ArrayList<Ingredient> ingredients = new ArrayList<>();
       ingredients.add(ingredientDao.findByName("Test1"));
       ingredients.add(ingredientDao.findByName("Test2"));

   	Pizza pizza = new Pizza();
       pizza.setName("Chorizo");
       pizza.setIngredients(ingredients);
       dao.insertPizzaWithIngredients(pizza);

     Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

     assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

     assertEquals("Chorizo", response.readEntity(String.class));
  }

  @Test
  public void testGetNotExistingPizzaName() {
    Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

    assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
  }
}
