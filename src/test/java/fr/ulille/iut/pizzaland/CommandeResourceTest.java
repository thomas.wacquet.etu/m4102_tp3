package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.PathParam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class CommandeResourceTest extends JerseyTest {
    private static final Logger LOGGER = Logger.getLogger(CommandeResourceTest.class.getName());
    private PizzaDao pizzaDao;
    private IngredientDao ingredientDao;
    private CommandeDao commandesDao;
    private static String username = "a";
    
    @Override
    protected Application configure() {
        BDDFactory.setJdbiForTests();

        return new ApiV1();

    }

    // Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
    // base de données
    // et les DAO

    // https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
    @Before
    public void setEnvUp() {
        pizzaDao = BDDFactory.buildDao(PizzaDao.class);
        pizzaDao.createTableAndIngredientAssociation();

        ingredientDao = BDDFactory.buildDao(IngredientDao.class);
        ingredientDao.createTable();
        
        // AJOUTE INGREDIENTS       
    	IngredientCreateDto test1 = new IngredientCreateDto();
    	test1.setName("Test1");
    	IngredientCreateDto test2 = new IngredientCreateDto();
    	test2.setName("Test2");
        target("/ingredients").request().post(Entity.json(test1));
        target("/ingredients").request().post(Entity.json(test2));
        
        
        // AJOUTE PIZZA
        ArrayList<Ingredient> ingredients = new ArrayList<>();
        ingredients.add(ingredientDao.findByName("Test1"));
        ingredients.add(ingredientDao.findByName("Test2"));

    	Pizza pizza = new Pizza();
        pizza.setName("Chorizo");
        pizza.setIngredients(ingredients);
        pizzaDao.insertPizzaWithIngredients(pizza);
        
 
        commandesDao = BDDFactory.buildDao(CommandeDao.class);
        commandesDao.createTable();
    }

    @After
    public void tearEnvDown() throws Exception {
    	commandesDao.dropTable();
    	ingredientDao.dropTable();
    	pizzaDao.dropTable();

    }

    @Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/commandes/" + username).request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        List<PizzaDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
        });
 
        assertEquals(0, pizzas.size());
    }
    @Test
    public void testGetExistingCommand() {
    	Pizza pizza = pizzaDao.findByName("Chorizo");
    	commandesDao.insertPizza("a", pizza);

        Response response = target("/commandes/" + username).request(MediaType.APPLICATION_JSON).get();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        
        List<PizzaDto> result = response.readEntity(new GenericType<List<PizzaDto>>() {
        });
        assertEquals(pizza, Pizza.fromDto(result.get(0)));
    }

    @Test
    public void testAddPizzaToCommand() {
    	Pizza pizza = pizzaDao.findByName("Chorizo");

        Response response = target("/commandes/" + username).request().post(Entity.json(Pizza.toDto(pizza)));
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());



        assertEquals(pizza, commandesDao.getAll(username).get(0));
        }
    @Test
    public void testUnexistingPizzas() {
        PizzaDto pizza = new PizzaDto();
        pizza.setName("Chorizo");

        Response response = target("/commandes").path(username).request().post(Entity.json(pizza));

        assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
    }

    @Test
    public void testDeleteExistingPizza() {
      Pizza pizza = pizzaDao.findByName("Chorizo");
      commandesDao.insertPizza(username, pizza);


      Response response = target("/commandes/" + username).path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      List<Pizza> result = commandesDao.getAll(username);
      assertEquals(0, result.size());
   }

   @Test
   public void testDeleteNotExistingPizza() {
     Response response = target("/commandes/" + username).path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }

}

