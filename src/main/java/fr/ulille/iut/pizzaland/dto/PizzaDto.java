package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	@Override
	public String toString() {
		return "PizzaDto [id=" + id + ", name=" + name + ", ingredients=" + ingredients + "]";
	}

	private UUID id;
    private String name;
    private List<Ingredient> ingredients;


	public PizzaDto() {
    }

	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    
}
