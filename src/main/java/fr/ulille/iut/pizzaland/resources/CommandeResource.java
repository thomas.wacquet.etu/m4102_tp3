package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;

@Produces("application/json")
@Path("/commandes")
public class CommandeResource {
    private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());

    private CommandeDao commandes;

    @Context
    public UriInfo uriInfo;

    public CommandeResource() {
        commandes = BDDFactory.buildDao(CommandeDao.class);
        commandes.createTable();
    }

    @GET
    @Path("{name}")
    public List<PizzaDto> getAll(@PathParam("name") String name) {
    	
        List<PizzaDto> l = commandes.getAll(name).stream().map(Pizza::toDto).collect(Collectors.toList());
        return l;
    }

    @POST
    @Path("{name}")
    public Response addPizza(@PathParam("name") String name, PizzaDto pizzaDto) {
        try {
        	Pizza pizza = Pizza.fromDto(pizzaDto);
            commandes.insertPizza(name, pizza);
            URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();
            return  Response.created(uri).entity(pizzaDto).build();
        } catch (Exception e) {
            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
        }
    }

    @DELETE
    @Path("{name}/{id}")
    public Response deletePizza(@PathParam("name") String name, @PathParam("id") UUID id) {
      try {

    	if (!commandes.remove(name, id)) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }

      return Response.status(Response.Status.ACCEPTED).build();
      } catch(Exception e) {
    	  e.printStackTrace();
          throw new WebApplicationException(Response.Status.NOT_FOUND);
      }
    }

}

