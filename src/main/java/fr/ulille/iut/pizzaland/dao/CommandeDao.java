package fr.ulille.iut.pizzaland.dao;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

  @SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (name VARCHAR(128) PRIMARY KEY, pid int, FOREIGN KEY(pid) REFERENCES pizzas(id))")
  void createTable();

  @SqlQuery("SELECT p.* FROM commandes as c inner join pizzas as p on c.pid = p.id where c.name = :name")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAll(@Bind("name") String name);

  @SqlUpdate("INSERT INTO commandes (name, pid) VALUES (:cname, :pizza.id)")
  boolean insertPizza(@Bind("cname") String cname, @BindBean("pizza") Pizza pizza);

  @SqlUpdate("DELETE FROM commandes WHERE name = :name and pid = :id")
  @RegisterBeanMapper(Pizza.class)
  boolean remove(@Bind("name") String name, @Bind("id") UUID id);

  @SqlUpdate("DROP TABLE commandes")
  void dropTable();

}