package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindList;
import org.jdbi.v3.sqlobject.statement.SqlBatch;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaIngredientsAssociation (    pid int NOT NULL,    iid int NOT NULL,    PRIMARY KEY(pid, iid),    FOREIGN KEY(iid) REFERENCES ingredients(id),    FOREIGN KEY(pid) REFERENCES pizzas(id))")
  void createAssociationTable();

  @Transaction
  default List<Pizza> getAll() {
	ArrayList<Pizza> pizzas = new ArrayList<>();

	for (Pizza pizza : getAllPizzas()) {
		pizza.setIngredients(getIngredientsForPizza(pizza.getId()));
		pizzas.add(pizza);
	}
	return pizzas;
  }

  @SqlQuery("SELECT * FROM pizzas")
  @RegisterBeanMapper(Pizza.class)
  List<Pizza> getAllPizzas();

  @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
  @RegisterBeanMapper(Pizza.class)
  Pizza findPizza(@Bind("id") UUID id);

  @Transaction
  default Pizza findById(UUID id){
	  Pizza pizza = findPizza(id);
	  if (pizza == null)
		  return null;
	  pizza.setIngredients(getIngredientsForPizza(id));
	  return pizza;
  }


  @Transaction
  default void insertPizzaWithIngredients(Pizza pizza) {
	  insertPizza(pizza);
	  insertIngredients(pizza.getId(), pizza.getIngredients());
  }

  @SqlBatch("INSERT INTO pizzaIngredientsAssociation (pid, iid) VALUES (:pid, :ingredient.id)")
  void insertIngredients(@Bind("pid") UUID pid, @BindBean("ingredient") List<Ingredient> ingredients);

  @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
  void insertPizza(@BindBean Pizza pizza);

  @Transaction
  default void remove(UUID id) {
	  removeId(id);
	  removeAssocId(id);
  }
  
  @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
  void removeId(@Bind("id") UUID id);

  @SqlUpdate("DELETE FROM pizzaIngredientsAssociation WHERE pid = :id")
  void removeAssocId(@Bind("id") UUID id);

  @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
  @RegisterBeanMapper(Pizza.class)
  Pizza findByName(@Bind("name") String name);

  @SqlQuery("SELECT * FROM ingredients as i inner join pizzaIngredientsAssociation as a on a.iid = i.id where a.pid = :id")
  @RegisterBeanMapper(Ingredient.class)
  List<Ingredient> getIngredientsForPizza(@Bind("id") UUID id);

  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }

  @Transaction
  default void dropTable() {
	  dropTablePizza();
	  dropAssocation();
  }
  @SqlUpdate("DROP TABLE pizzas")
  void dropTablePizza();
  @SqlUpdate("DROP TABLE pizzaIngredientsAssociation")
  void dropAssocation();


}