| URI                      | Opération    |  MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (P2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (P2) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>-> | Pizza (P1) | Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom) ou si a moins un des ingredients de la pizza n'existe pas |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |


Représentation JSON P1:
```
{
  "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
  "name": "mozzarella",
  "ingredients": ["f38806a8-7c85-49ef-980c-149dcd81d309"]
}
```

Représentation JSON P2
```
{
  "name": "mozzarella",
  "ingredients": ["f38806a8-7c85-49ef-980c-149dcd81d309"]
}
```
